/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.samples.deployment.remote.client;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.samples.deployment.remote.service.RemoteDeploymentService;
import org.osgi.framework.Version;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class RemoteDeploymentServiceClient implements Runnable {

    // adopted debug option from ACE ma
    private static final boolean QUIET = Boolean.parseBoolean(System.getProperty("quiet", "false"));

    // dm injected
    private volatile RemoteDeploymentService m_deploymentService;
    private ScheduledExecutorService m_exe;

    public void start() throws Exception {
        m_exe = Executors.newSingleThreadScheduledExecutor();
        m_exe.scheduleAtFixedRate(this, 5, 5, TimeUnit.SECONDS);
        if (!QUIET)
            System.err.println("RemoteDeploymentServiceClient: started");
    }

    public void stop() throws Exception {
        m_exe.shutdownNow();
        m_exe = null;
        if (!QUIET)
            System.err.println("RemoteDeploymentServiceClient: stopped");
    }

    public void run() {
        try {
            Version remoteVersion = Version.parseVersion(m_deploymentService.getHighestRemoteVersion());
            Version localVersion = Version.parseVersion(m_deploymentService.getHighestLocalVersion());
            if (remoteVersion != null && (localVersion == null || remoteVersion.compareTo(localVersion) > 0)) {
                if (!QUIET)
                    System.err.println("Client: Requesting update from " + localVersion + " to " + remoteVersion);
                m_deploymentService.update(remoteVersion.toString());
            }
        }
        catch (Exception e) {}
    }
}
