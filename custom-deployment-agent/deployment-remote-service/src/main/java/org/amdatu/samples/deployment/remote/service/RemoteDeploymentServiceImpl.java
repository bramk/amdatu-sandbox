/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.samples.deployment.remote.service;

import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.ace.deployment.service.DeploymentService;
import org.osgi.framework.Version;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class RemoteDeploymentServiceImpl implements RemoteDeploymentService {

    // adopted debug option from ACE ma
    private static final boolean QUIET = Boolean.parseBoolean(System.getProperty("quiet", "false"));

    // dm injected
    private volatile DeploymentService m_deploymentService;

    private volatile ScheduledExecutorService m_executorService;
    private volatile LinkedBlockingQueue<InstallVersionRequest> m_installVersionRequestQueue;

    private volatile boolean m_updateOnce = false;

    public void init() throws Exception {
        // support forcing runonce at ma startup
        String updateOnce = System.getProperty("updateOnce");
        if (updateOnce != null) {
            m_updateOnce = Boolean.parseBoolean(updateOnce);
        }
        if (!QUIET)
            System.err.println("RemoteDeploymentService: initialized");
    }

    public void start() throws Exception {
        m_installVersionRequestQueue = new LinkedBlockingQueue<InstallVersionRequest>();
        m_executorService = Executors.newSingleThreadScheduledExecutor();
        m_executorService.scheduleAtFixedRate(new UpdateRunnable(), 5, 5, TimeUnit.SECONDS);
        if (!QUIET)
            System.err.println("RemoteDeploymentService: started");
    }

    public void stop() throws Exception {
        m_executorService.shutdownNow();
        m_executorService = null;
        m_installVersionRequestQueue = null;
        if (!QUIET)
            System.err.println("RemoteDeploymentService: stopped");
    }

    public String getHighestLocalVersion() throws Exception {
        Version version = m_deploymentService.getHighestLocalVersion();
        return version == null ? null : version.toString();
    }

    public String getHighestRemoteVersion() throws Exception {
        Version version = m_deploymentService.getHighestRemoteVersion();
        return version == null ? null : version.toString();
    }

    public SortedSet<String> getRemoteVersions() throws IOException {
        SortedSet<Version> versions = m_deploymentService.getRemoteVersions();
        SortedSet<String> strversions = new TreeSet<String>();
        if (versions != null) {
            for (Version v : versions) {
                strversions.add(v.toString());
            }
        }
        return versions == null ? null : strversions;
    }

    public void installVersion(String remoteVersion, String localVersion) throws Exception {
        Version rversion = Version.parseVersion(remoteVersion);
        if (rversion.equals(Version.emptyVersion)) {
            throw new Exception("Not a valid remote version: " + remoteVersion);
        }
        Version lversion = Version.parseVersion(localVersion);
        if (lversion.equals(Version.emptyVersion)) {
            throw new Exception("Not a valid local version: " + localVersion);
        }
        m_installVersionRequestQueue.put(new InstallVersionRequest(rversion, lversion));
    }

    public void update(String toVersion) throws Exception {
        Version version = Version.parseVersion(toVersion);
        if (toVersion.equals(Version.emptyVersion)) {
            throw new Exception("Not a valid version: " + toVersion);
        }
        m_installVersionRequestQueue.put(new InstallVersionRequest(version, m_deploymentService.getHighestLocalVersion()));
    }

    class UpdateRunnable implements Runnable {

        public void run() {
            DeploymentService deploymentService = m_deploymentService;
            if (deploymentService == null) {
                return;
            }
            try {
                Version localVersion = deploymentService.getHighestLocalVersion();
                if (localVersion == null || m_updateOnce) {
                    Version remoteVersion = deploymentService.getHighestRemoteVersion();
                    if (remoteVersion != null && (localVersion == null || remoteVersion.compareTo(localVersion) > 0)) {
                        if (!QUIET)
                            System.err.println("RemoteDeploymentService: Executing forced update from " + localVersion
                                + " to " + remoteVersion);
                        deploymentService.update(remoteVersion);
                        m_updateOnce = false;
                    }
                }
                else {
                    InstallVersionRequest task = m_installVersionRequestQueue.poll();
                    if (task != null) {
                        if (!QUIET)
                            System.err.println("RemoteDeploymentService: Executing requested update from "
                                + task.getRemoteVersion()
                                + " to " + task.getLocalVersion());
                        deploymentService.installVersion(task.getRemoteVersion(), task.getLocalVersion());
                    }
                }
            }
            catch (Exception e) {}

        }
    }

    class InstallVersionRequest {

        private Version m_remoteVersion;
        private Version m_localVersion;

        public InstallVersionRequest(Version remoteVersion, Version localVersion) {
            m_remoteVersion = remoteVersion;
            m_localVersion = localVersion;
        }

        public Version getLocalVersion() {
            return m_localVersion;
        }

        public Version getRemoteVersion() {
            return m_remoteVersion;
        }
    }
}
