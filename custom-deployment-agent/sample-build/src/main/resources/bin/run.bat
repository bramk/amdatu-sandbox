@echo off

set JAVA_OPTS=
set JAVA_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n
set JAVA_OPTS=%JAVA_OPTS% -Xms256m -Xmx1024m -XX:MaxPermSize=256m
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=utf-8
set JAVA_OPTS=%JAVA_OPTS% -Dorg.apache.ace.deployment.task=disabled
set JAVA_OPTS=%JAVA_OPTS% -DupdateOnce=true
set JAVA_OPTS=%JAVA_OPTS% -cp lib/org.apache.ace.launcher-0.8.1-SNAPSHOT.jar
set JAVA_OPTS=%JAVA_OPTS%;lib/org.amdatu.samples.deployment.remote.service-1.0-SNAPSHOT.jar

set ACE_OPTS=
set ACE_OPTS=%ACE_OPTS% fwOption=org.osgi.framework.system.packages.extra=org.amdatu.samples.deployment.remote.service;managementagent=true;mandatory:=managementagent;version="1.0"
set ACE_OPTS=%ACE_OPTS% bundle=org.amdatu.samples.deployment.remote.service.Activator
set ACE_OPTS=%ACE_OPTS% discovery=http://localhost:8080
set ACE_OPTS=%ACE_OPTS% identification=AMA-X1

echo JAVA_OPTS: %JAVA_OPTS%
echo ACE_OPTS : %ACE_OPTS%

java %JAVA_OPTS% org.apache.ace.launcher.Main %ACE_OPTS%

